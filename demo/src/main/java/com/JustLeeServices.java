package com;
import java.sql.*;
import java.util.Scanner;

public class JustLeeServices {

    public static void main(String[] args) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter user: ");
        String user = sc.nextLine();
        System.out.println("Enter password: ");
        String password = sc.nextLine();
       
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try {
            Connection con = null;
            // Connecting to database
            //Class.forName("com.mysql.jdbc.Driver"); 
            con = DriverManager.getConnection(url, user, password);
            if (con != null) {
                System.out.println("This works connection success");
            }

            Publisher pId = new Publisher("6", "DC", "Jack Smith", "438-202-0972");
            //TODOaddPublisher(con, pId);
            Books book = new Books("0201540733", "Database2", "14-OCT-24", "1", 100.00, 95.00,10.00, "Computers"); 
            addBook(con,book);
            Books newBook = getBooks(con,"0201540733");
            System.out.println(newBook.toString());

        } catch (SQLException e) {
            System.out.println("Something went wrong.");
            e.printStackTrace();
        }
        

    }


    public static Books getBooks(Connection con, String isbn) throws SQLException {
        Books book = null;
        PreparedStatement statement = null;
        try{
            statement = con.prepareStatement("select * from books where isbn = ?");
            statement.setString(1, isbn);
            ResultSet r = statement.executeQuery();
            while (r.next()){
                book = new Books(isbn, r.getString("Title"), r.getString("PubDate"), r.getString("PubID"), r.getDouble("Cost"), r.getDouble("Retail"), r.getDouble("Discount"), r.getString("Category"));
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            if (statement != null && !statement.isClosed()){
                statement.close();
            }
        }
        return book;
    }

    public static void addBook(Connection con, Books book) throws SQLException {
        String sql = "INSERT INTO books VALUES (?,?,?,?,?,?,?,?)";
        try (PreparedStatement statement = con.prepareStatement(sql)){
            statement.setString(1, book.getIsbn());
            statement.setString(2, book.getTitle());
            statement.setString(3, book.getpubDate());
            statement.setString(4, book.getpubId());
            statement.setDouble(5, book.getCost());
            statement.setDouble(6, book.getRetail());
            statement.setDouble(7, book.getDiscount());
            statement.setString(8, book.getCategory());
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

    public static void addPublisher(Connection con, Publisher pub) throws SQLException {
        String sql = "INSERT INTO Publisher VALUES (?,?,?,?)";
        try (PreparedStatement statement = con.prepareStatement(sql)){
            statement.setString(1, pub.getPubId());
            statement.setString(2, pub.getName());
            statement.setString(3, pub.getContact());
            statement.setString(4, pub.getNumber());
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public static Publisher getPublisher(Connection con, String pubId){
        Publisher pb = null;
        try
           (PreparedStatement statement = con.prepareStatement("SELECT * FROM Publisher WHERE PubId = ?")){
               statement.setString(1, pubId);
               ResultSet result = statement.executeQuery();
               while(result.next()){
                   pb = new Publisher(pubId, result.getString("name"),
                   result.getString("Contact"), result.getString("Phone"));
               }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return pb;
    }

}
