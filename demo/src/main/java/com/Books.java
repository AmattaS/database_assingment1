package com;

public class Books {
    private String isbn;
    private String  title;
    private String pubDate;
    private String pubId;
    private double cost;
    private double retail;
    private double discount;
    private String category;
    

    public Books( String isbn, String title, String pDate, String pId, double cost, double retail, double discount, String cat ) {
        if (isbn == null) {System.out.println("Isbn cannot be null");}
        this.isbn = isbn;
        this.title = title;
        this.pubDate = pDate;
        this.pubId = pId;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = cat; 
    }
    
    public String getIsbn() {
        return this.isbn;
    }
    public String getTitle() {
        return this.title;
    }
    public String getpubDate() {
        return this.pubDate;
    }
    public String getpubId() {
        return this.pubId;
    }
    public double getCost() {
        return this.cost;
    }
    public double getRetail() {
        return this.retail;
    }
    public double getDiscount() {
        return this.discount;
    }
    public String getCategory() {
        return this.category;
    }
    @Override
    public String toString() {
        return "Books [ Isbn: "+this.isbn+", title: "+this.title+", pubDate: "+this.pubDate+", pubId: "+this.pubId+", cost: "+this.cost+", retail: "+this.retail+", discount: "+this.discount+", category: "+this.category;
    }
}
