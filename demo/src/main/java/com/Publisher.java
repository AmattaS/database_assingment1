package com;

public class Publisher {
    private String pubId;
    private String name;
    private String contact;
    private String number;


    public Publisher(String pubId, String name, String contact, String number) {
        if(pubId == null ) {System.out.println("pubId cannot be null");}
        this.pubId = pubId;
        this.name = name;
        this.contact = contact;
        this.number = number;
    }

    public String getPubId() {
        return this.pubId;
    }

    public String getName() {
        return this.name;
    }

    public String getContact() {
        return this.contact;
    }

    public String getNumber() {
        return this.number;
    }
    
    @Override
    public String toString() {
        return "Publisher[ pubId: "+this.pubId+ ", name: "+this.name+", contact: "+this.contact+", number: "+this.number;
    }
}